# TP3 : Self-hosted private cloud platform
## 1. Architecture

| Node           | IP Address  | Rôle                         |
|----------------|-------------|------------------------------|
| `frontend.one` | `10.3.1.11` | WebUI OpenNebula             |
| `kvm1.one`     | `10.3.1.21` | Hyperviseur + Endpoint VXLAN |
| `kvm2.one`     | `10.3.1.22` | Hyperviseur + Endpoint VXLAN |

**Configuration de mon Vagrantfile pour qu'il puisse lancer les 3 VM en même temps**

```powershell 
Vagrant.configure("2") do |config|
  config.vm.define "frontend" do |frontend|
    frontend.vm.box = "rocky/update"
    frontend.vm.network "private_network", ip: "10.3.1.11"
    frontend.vm.hostname = "frontend.one"
    frontend.vm.provider "virtualbox" do |vb|
      vb.name = "frontend.one"
      vb.memory = "2048"
      vb.cpus = 2
      vb.customize ['modifyvm', :id, '--nested-hw-virt', 'on']
    end
  end
 
  config.vm.define "kvm1" do |kvm1|
    kvm1.vm.box = "rocky/update"
    kvm1.vm.network "private_network", ip: "10.3.1.21"
    kvm1.vm.hostname = "kvm1"
    kvm1.vm.provider "virtualbox" do |vb|
      vb.name = "kvm1.one"
      vb.memory = "2048"
      vb.cpus = 2
      vb.customize ['modifyvm', :id, '--nested-hw-virt', 'on']
    end
  end
 
  config.vm.define "kvm2" do |kvm2|
    kvm2.vm.box = "rocky/update"
    kvm2.vm.network "private_network", ip: "10.3.1.22"
    kvm2.vm.hostname = "kvm2"
    kvm2.vm.provider "virtualbox" do |vb|
      vb.name = "kvm2.one"
      vb.memory = "2048"
      vb.cpus = 2
      vb.customize ['modifyvm', :id, '--nested-hw-virt', 'on']
    end
  end
end
```
```powershell 
PS D:\Efrei\coursleo\tp3\tp3OpenNebula> vagrant status
Current machine states:

frontend                  running (virtualbox)
kvm1                      running (virtualbox)
kvm2
```
# PARTIE CONFIG FRONTEND
## A. Database
```powershell
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'Nelok123*%';
Query OK, 0 rows affected (0.01 sec)

mysql> CREATE USER 'oneadmin' IDENTIFIED BY 'Nelok1234*%';
Query OK, 0 rows affected (0.02 sec)

mysql> CREATE USER 'oneadmin' IDENTIFIED BY 'Nelok1234*%';
Query OK, 0 rows affected (0.02 sec)

mysql> CREATE DATABASE opennebula;
Query OK, 1 row affected (0.01 sec)

mysql> GRANT ALL PRIVILEGES ON opennebula.* TO 'oneadmin';
Query OK, 0 rows affected (0.00 sec)

mysql> SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
Query OK, 0 rows affected (0.00 sec)

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| opennebula         |
| performance_schema |
| sys                |
+--------------------+
```
## B. OpenNebula
🌞 **Ajouter les dépôts Open Nebula** 

```powershell 
[vagrant@frontend ~]$ sudo nano /etc/yum.repos.d/opennebula.repo
[vagrant@frontend ~]$ sudo dnf makecache -y
[vagrant@frontend ~]$ sudo dnf install opennebula opennebula-sunstone opennebula
-fireedge
```
🌞 **Installer OpenNebula**
`sudo dnf install opennebula opennebula-sunstone opennebula-fireedge`

🌞 **Configuration OpenNebula**
Dans le fichier `/etc/one/oned.conf`
```powershell
DB = [ 
    BACKEND = "mysql",
    SERVER  = "localhost",                                                    
    PORT    = 0,
    USER    = "oneadmin",                               
    PASSWD  = "Nelok1234*%",
    DB_NAME = "opennebula",                                                   
    CONNECTIONS = 25,                                                      
    COMPARE_BINARY = "no" ]
```

🌞 **Créer un user pour se log sur la WebUI OpenNebula**

Connexion a to oneadmin : `sudo su - oneadmin`
Création du user : 
```powershell
[oneadmin@frontend ~]$ cat /var/lib/one/.one/one_auth
oneadmin:toto
```

🌞 **Démarrer les services OpenNebula**

- démarrez les services `opennebula`, `opennebula-sunstone` : `sudo systemctl start opennebula opennebula-sunstone`
- activez-les aussi au démarrage de la machine : ` sudo systemctl enable opennebula opennebula-sunstone`

## C. Conf système

🌞 **Ouverture firewall**
**Ouverture des ports**
```powershell
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=9869/tcp --permanent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=22/tcp --perman
ent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=2633/tcp --perm
anent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=4124/tcp --perm
anent
success
[vagrant@frontend ~]$ sudo firewall-cmd --zone=public --add-port=29876/tcp --per
manent
success
```
## D. Test

**RDV sur `http://10.3.1.11:9869`**
J'ai pu me connecter avec le user : oneadmin et le password :toto

![alt text](image-1.png)


# II. 2. Noeuds KVM

🌞 **Ajouter des dépôts supplémentaires de nebula**
`sudo nano /etc/yum.repos.d/opennebula.repo`

```
[opennebula]
name=OpenNebula Community Edition
baseurl=https://downloads.opennebula.io/repo/6.8/RedHat/$releasever/$basearch
enabled=1
gpgkey=https://downloads.opennebula.io/repo/repo2.key
gpgcheck=1
repo_gpgcheck=1
```
puis `sudo dnf makecache -y`

## B. Système

🌞 **Ouverture firewall**
les deux commandes : 
```powershell
sudo firewall-cmd --zone=public --add-port=22/tcp --permanent
sudo firewall-cmd --zone=public --add-port=8472/udp --permanent
```


🌞 **Handle SSH**

Création d'une key avec `ssh-keygen`

On met la clef publique de frontend dans kvm1
```powershell
[oneadmin@kvm1 .ssh]$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDD3xOOvj7yQnjwGbeejb7dO/uSGeNxOTZlYutV0MfM+oEiq3v1zZO1r5N+wn/5nITlzhuQyEANitPQUQ6f1Hz9AuPIQ5lR4R/2LpC6N+9EVHDMiggPK7SukfWoTQqgmhux4MazJSERkN6A5WoaAxmJ3EDnkPLQhoheTPScnzl94jSdSFiWKgnnMAL6W9LNK67fCH1PPsNahJkt3RXNPeE6G+BHlKSonJgZjAW9Z/Sg1ztFP+RkYISL8QiY/xPsTJKZnlDKYimqB3Q/JXvJg6qWlbAAIQKG2UTiKlABxPknd0imllRVbp4TXgmsaG5dIrt2CGi3I09UEryYuqJAj8RFwkljKJGMjJZcKyxbCTyVqupyDWJkIT3tHpBx5aP6FIki+ORN8kUU6qnJDWXcoc7lsMScAvFCfxNUEvAZPB6JLACB3/KeDhi9by9BOrgXk1RxwfbPOIx3rOFxbHHT0E7CwkaX/svdYdNSyu3XdaPIuxhQN+Kus5W2e/T2ohGEpws= oneadmin@frontend.one
```

## C. Ajout des noeuds au cluster

➜ **RDV de nouveau sur la WebUI de OpenNebula, et naviguez dans `Infrastructure > Hosts`**

Ajoutez le nouvel hôte KVM et assurez-vous qu'ils remontent en statut `ON`.

![alt text](image-2.png)

# II. 3. Setup réseau

## B. Création du Virtual Network
![alt text](image-3.png)

## C. Préparer le bridge réseau
```powershell
[vagrant@kvm1 ~]$ sudo ip link add name vxlan_bridge type bridge
[vagrant@kvm1 ~]$ sudo ip link set dev vxlan_bridge up
[vagrant@kvm1 ~]$ sudo ip addr add 10.220.220.201/24 dev vxlan_bridge
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
success
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[vagrant@kvm1 ~]$ sudo firewall-cmd --reload
success
```

# III. Utiliser la plateforme
➜ **RDV de nouveau sur la WebUI de OpenNebula, et naviguez dans `Settings > Onglet Auth`**

- OpenNebula a généré une paire de clé sur la machine `frontend.one`
- elle se trouve dans le dossier `.ssh` dans le homedir de l'utilisateur `oneadmin`
- déposez la clé publique dans cet interface de la WebUI

![alt text](image-4.png)

➜ **Toujours sur la WebUI de OpenNebula, naviguez dans `Storage > Apps`**

Récupérez l'image de Rocky Linux 9 dans cette interface.

![alt text](image-5.png)

Petite aparté, lorsque je lançais une vm depuis openNebula, ça ne marchait pas donc j'ai : 

**Sur fronted et kvm1**
```powershell 
cp id_rsa.pub authorized_keys (only frontend)
ssh-keyscan frontend frontend.one kvm1.one kvm1 10.3.1.11 10.3.1.21 > known_hosts 
```


J'ai aussi rajouter dans le fichier `/etc/hosts` de frontend et kvm1 : 
```
10.3.1.11 frontend frontend.one
10.3.1.21 kvm1 kvm1.one
```



➜ **Toujouuuuurs sur la WebUI de OpenNebula, naviguez dans `Instances > VMs`**

![alt text](image-6.png)

➜ Tester la connectivité à la VM

déjà est-ce qu'on peut la ping ?

depuis le noeud kvm1.one, faites un ping vers l'IP de la VM
, l'IP de la VM est visible dans la WebUI
```powershell
[oneadmin@kvm1 ~]$ ping 10.220.220.10
PING 10.220.220.10 (10.220.220.10) 56(84) bytes of data.
64 bytes from 10.220.220.10: icmp_seq=1 ttl=64 time=1.05 ms
64 bytes from 10.220.220.10: icmp_seq=2 ttl=64 time=0.411 ms
64 bytes from 10.220.220.10: icmp_seq=3 ttl=64 time=0.447 ms
64 bytes from 10.220.220.10: icmp_seq=4 ttl=64 time=0.344 ms
64 bytes from 10.220.220.10: icmp_seq=5 ttl=64 time=0.299 ms
^C
--- 10.220.220.10 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4101ms
rtt min/avg/max/mdev = 0.299/0.509/1.046/0.273 ms
```
## Se connecter en ssh a la vm qu'on vient de creer via opennebula

```powershell
# connectez vous en SSH sur la machine frontend.one
❯ vagrant ssh frontend

# devenez l'utilisateur oneadmin
[vagrant@frontend ~]$ sudo su - oneadmin

# lancez un agent SSH (demandez-moi si vous voulez une explication sur ça)
[oneadmin@frontend ~]$ eval $(ssh-agent)

# ajoutez la clé privée à l'agent SSH
[oneadmin@frontend ~]$ ssh-add
Identity added: /var/lib/one/.ssh/id_rsa (oneadmin@frontend)

# se connecter à kvm1 en faisant suivre l'agent SSH
[oneadmin@frontend ~]$ ssh -A 10.3.1.21

# depuis kvm1, se connecter à la VM, sur l'utilisateur root
[oneadmin@kvm1 ~]$ ssh root@10.220.220.1 
```

**Résultat : je peux me connecter en ssh a la vm** 

```powershell
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 02:00:0a:dc:dc:0a brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    altname ens3
    inet 10.220.220.10/24 brd 10.220.220.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::aff:fedc:dc0a/64 scope link
       valid_lft forever preferred_lft forever
```

**Avoir internet**
```powershell
[root@localhost ~]# ip route add default via 10.220.220.201
[root@localhost ~]# ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=58 time=20.0 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=58 time=18.4 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=58 time=20.2 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=58 time=19.3 ms
^C
--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3011ms
rtt min/avg/max/mdev = 18.379/19.465/20.167/0.713 ms
```

# IV. Ajouter d'un noeud et VXLAN

# 1. Ajout d'un noeud
🌞 **Setup de `kvm2.one`, à l'identique de `kvm1.one`** excepté :

comme d'hab, on ajoute la clef publique de frontend dans authorized keys de kvm2
puis on ajoute 10.3.1.22 kvm2.one kvm2 dans le /etc/hosts de vagrant@frontend, puis on ssh-keyscan frontend frontend.one kvm2.one kvm2 10.3.1.11 10.3.1.22 > known_hosts depuis oneadmin@frontend

Une fois kvm2 ajouter sur openNebula ainsi que la création d'une nouvelle vm dans le réseau. On peut s'y connecter via : 

```powershell
[vagrant@frontend ~]$ sudo su - oneadmin
Last login: Wed Apr 10 10:12:41 UTC 2024 on pts/0
[oneadmin@frontend ~]$ eval $(ssh-agent)
Agent pid 16014
[oneadmin@frontend ~]$ ssh-add
Identity added: /var/lib/one/.ssh/id_rsa (oneadmin@frontend.one)
[oneadmin@frontend ~]$ ssh -A 10.3.1.22
Last login: Wed Apr 10 10:29:51 2024 from 10.3.1.11
[oneadmin@kvm2 ~]$  ssh root@10.220.220.11
[root@localhost ~]# ls
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 02:00:0a:dc:dc:0b brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    altname ens3
    inet 10.220.220.11/24 brd 10.220.220.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::aff:fedc:dc0b/64 scope link
       valid_lft forever preferred_lft forever
```

🌞 **Les deux VMs doivent pouvoir se ping**
```powershell
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 02:00:0a:dc:dc:0a brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    altname ens3
    inet 10.220.220.10/24 brd 10.220.220.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::aff:fedc:dc0a/64 scope link
       valid_lft forever preferred_lft forever
[root@localhost ~]# ping 10.220.220.11
PING 10.220.220.11 (10.220.220.11) 56(84) bytes of data.
64 bytes from 10.220.220.11: icmp_seq=1 ttl=64 time=1.50 ms
64 bytes from 10.220.220.11: icmp_seq=2 ttl=64 time=1.20 ms
^C
--- 10.220.220.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 1.201/1.351/1.502/0.150 ms
```

**Les deux vm se ping entre elles !**

🌞 Téléchargez tcpdump sur l'un des noeuds KVM
effectuez deux captures, pendant que les VMs sont en train de se ping :

une qui capture le trafic de l'interface réelle : eth1 probablement (celle qui a l'IP host-only, celle qui porte 10.3.1.22 sur kvm2 par exemple)
