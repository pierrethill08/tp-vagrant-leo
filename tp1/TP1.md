# TP1

## 0. Setup

Pour ce TP, nous utiliserons VirtualBox comme hyperviseur local et Vagrant pour piloter les machines virtuelles. Assurez-vous d'avoir installé VirtualBox et Vagrant sur votre poste.

### I. Une première VM

#### 1. ez startup

Commencez par créer un répertoire de travail et initialiser un fichier Vagrantfile pour une VM Rocky 9 :
![alt text](image.png)

#### 2. Un peu de conf

🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :

- ait l'IP `10.1.1.11/24`
- porte le hostname `ezconf.tp1.efrei`
- porte le nom (pour Vagrant) `ezconf.tp1.efrei` (ce n'est pas le hostname de la machine)
- ait 2G de RAM
- ait un disque dur de 20G

```bash
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "ezconf.tp1.efrei"
    vb.memory = "2048"
    vb.cpus = 2
  end
  config.vm.disk :disk, size: "20GB", primary: true
end
```
# II. Initialization script
 **Ajustez le `Vagrantfile`** :

- quand la VM démarre, elle doit exécuter un script bash
- le script installe les paquets `vim` et `python3`
- il met aussi à jour le système avec un `dnf update -y` (si c'est trop long avec le réseau de l'école, zappez cette étape)
- ça se fait avec une ligne comme celle-ci :

```Vagrantfile
# on suppose que "script.sh" existe juste à côté du Vagrantfile
config.vm.provision "shell", path: "script.sh" 
```


 Vagrantfile fichier modifier :

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "ezconf.tp1.efrei"
    vb.memory = "2048"
    vb.cpus = 2
  end
  config.vm.disk :disk, size: "20GB", primary: true
  config.vm.provision "shell", path: "script.sh"
end

```
Dans le script.sh :

```shell
#!/bin/bash

# Update the system (optional, you can skip if it takes too long)
# dnf update -y

# Install vim and python3
dnf install -y vim python3 gedit nano 
```
**Avant le script** 

![alt text](image-1.png)

**Après le script** 
![alt text](image-2.png)

# III. Repackaging

Pour accélérer le déploiement, mais aussi pour intégrer une conf dès le boot de la VM, on peut **repackager les boxes avec Vagrant.**

C'est à dire qu'on peut créer un template de VM quoi : **on crée une image custom qui contient déjà la conf, et on a plus qu'à allumer des VMs à partir de cette image.**

Une fois qu'une VM est allumée, qu'on y a fait un peu de conf, on peut à tout moment la transformer en un nouveau template (une nouvelle "box" au sens de Vagrant).
Et on pourra donc créer de nouvelles VMs qui contiennent déjà cette conf.

La marche à suivre pour faire ça est la suivante :

```bash
# toujours depuis le même répertoire, avec la VM allumée
$ vagrant package --output rocky-efrei.box

# on ajoute le fichier .box produit à la liste des box que gère Vagrant
$ vagrant box add rocky-efrei rocky-efrei.box

# la box est visible dans la liste des box Vagrant
$ vagrant box list
```
![alt text](image-4.png)

🌞 **Repackager la VM créée précédemment**

- comme ça vous aurez une box qui contient un OS déjà à jour, avec quelques paquets préinstallés
- donnez moi la suite de commande dans le compte-rendu de TP

```powershell
PS D:\Efrei\coursleo\tp1provisionning\rocky-efrei> vagrant init rocky-efrei

A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.

PS D:\Efrei\coursleo\tp1provisionning\rocky-efrei> vagrant up

Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'rocky-efrei'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: rocky-efrei_default_1712149862237_69549
Vagrant is currently configured to create VirtualBox synced folders with
the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
guest is not trusted, you may want to disable this option. For more
information on this option, please refer to the VirtualBox manual:

  https://www.virtualbox.org/manual/ch04.html#sharedfolders

This option can be disabled globally with an environment variable:

  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

or on a per folder basis within the Vagrantfile:

  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 6.1.48
    default: VirtualBox Version: 7.0
==> default: Mounting shared folders...
    default: /vagrant => D:/Efrei/coursleo/tp1provisionning/rocky-efrei

PS D:\Efrei\coursleo\tp1provisionning\rocky-efrei> vagrant halt
==> default: Attempting graceful shutdown of VM...

PS D:\Efrei\coursleo\tp1provisionning\rocky-efrei> vagrant package --output rocky-efrei-repackage.box
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: D:/Efrei/coursleo/tp1provisionning/rocky-efrei/rocky-efrei-repackage.box

PS D:\Efrei\coursleo\tp1provisionning\rocky-efrei> vagrant box add rocky-efrei-repackage rocky-efrei-repackage.box

==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'rocky-efrei-repackage' (v0) for provider:
    box: Unpacking necessary files from: file://D:/Efrei/coursleo/tp1provisionning/rocky-efrei/rocky-efrei-repackage.box
    box:
==> box: Successfully added box 'rocky-efrei-repackage' (v0) for ''!

PS D:\Efrei\coursleo\tp1provisionning\rocky-efrei> vagrant box list

generic/rocky9        (virtualbox, 4.3.12, (amd64))
rocky-efrei           (virtualbox, 0)
rocky-efrei-repackage (virtualbox, 0)
```

# IV. Multi VM

Il est -évidemment- possible de créer plusieurs VMs à l'aide d'un seul `Vagrantfile` et donc de une seule commande `vagrant up`.

```powershell 

PS D:\Efrei\coursleo\tp1provisionning\multiVM> vagrant init

```

**VagrantFile pour lancer les deux VM en même temps**

```
Vagrant.configure("2") do |config|
 
  config.vm.define "node1" do |node1|
    node1.vm.box = "generic/rocky9"
    node1.vm.network "private_network", ip: "10.1.1.101"
    node1.vm.hostname = "node1.tp1.efrei"
    node1.vm.provider "virtualbox" do |vb|
      vb.name = "node1.tp1.efrei"
      vb.memory = "2048"
      vb.cpus = 2
    end
  end
  

  config.vm.define "node2" do |node2|
    node2.vm.box = "generic/rocky9"
    node2.vm.network "private_network", ip: "10.1.1.102"
    node2.vm.hostname = "node2.tp1.efrei"
    node2.vm.provider "virtualbox" do |vb|
      vb.name = "node2.tp1.efrei"
      vb.memory = "1024"
      vb.cpus = 1
    end
  end
end
```
**Machine run**

```powershell
PS D:\Efrei\coursleo\tp1provisionning\multiVM> vagrant status
Current machine states:

node1                     running (virtualbox)
node2                     running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
PS D:\Efrei\coursleo\tp1provisionning\multiVM>
```


**ping node1 -> node2 :**

```powershell
PS D:\Efrei\coursleo\tp1provisionning\multiVM> vagrant ssh node1
[vagrant@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cb:b3:b6 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute eth0
       valid_lft 85627sec preferred_lft 85627sec
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:fb:49:98 brd ff:ff:ff:ff:ff:ff
    altname enp0s8
    inet 10.1.1.101/24 brd 10.1.1.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fefb:4998/64 scope link
       valid_lft forever preferred_lft forever
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=0.485 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=1.30 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=1.03 ms
^C
--- 10.1.1.102 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2304ms
rtt min/avg/max/mdev = 0.485/0.935/1.295/0.336 ms
[vagrant@node1 ~]$
```

# V. cloud-init

La commande n'était pas disponible, j'ai utiliser votre iso 

**J'ai installer cloud-init sur ma VM**
Je l'ai repackager

```powershell
PS D:\Efrei\coursleo\tp1provisionning\vagrantcloud> vagrant package --output tp5.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: D:/Efrei/coursleo/tp1provisionning/vagrantcloud/tp5.box
PS D:\Efrei\coursleo\tp1provisionning\vagrantcloud> vagrant box add tp5 tp5.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'tp5' (v0) for provider:
    box: Unpacking necessary files from: file://D:/Efrei/coursleo/tp1provisionning/vagrantcloud/tp5.box
    box:
==> box: Successfully added box 'tp5' (v0) for ''!
PS D:\Efrei\coursleo\tp1provisionning\vagrantcloud> vagrant box list
generic/rocky9          (virtualbox, 4.3.12, (amd64))
rocky-efrei             (virtualbox, 0)
rocky-efrei-repack-exo5 (virtualbox, 0)
rocky-efrei-repackage   (virtualbox, 0)
tp5                     (virtualbox, 0)
PS D:\Efrei\coursleo\tp1provisionning\vagrantcloud> vagrand up
```

**Mon Vagrantfile :**
```
Vagrant.configure("2") do |config|
  config.vm.box = "tp5"
  config.vm.disk :dvd, name: "installer", file: "./cloud-init.iso"
end
```
Ce qu'il y a dans

```powershell 
PS D:\Efrei\coursleo\tp1provisionning\vagrantcloud> vagrant ssh
Last login: Wed Apr  3 16:33:39 2024 from 10.0.2.2
[vagrant@cloud ~]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
sssd:x:998:995:User for sssd:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/:/usr/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/sbin/nologin
chrony:x:997:994:chrony system user:/var/lib/chrony:/sbin/nologin
systemd-oom:x:992:992:systemd Userspace OOM Killer:/:/usr/sbin/nologin
vagrant:x:1000:1000::/home/vagrant:/bin/bash
vboxadd:x:991:1::/var/run/vboxadd:/sbin/nologin
polkitd:x:990:990:User for polkitd:/:/sbin/nologin
rtkit:x:172:172:RealtimeKit:/proc:/sbin/nologin
geoclue:x:989:989:User for geoclue:/var/lib/geoclue:/sbin/nologin
pipewire:x:988:988:PipeWire System Daemon:/var/run/pipewire:/sbin/nologin
flatpak:x:987:987:User for flatpak system helper:/:/sbin/nologin
lala:x:1001:1001:Super adminsys:/home/lala:/bin/bash
```