# TP2 : Network boot

Je recree une box du nom de tp2/rocky.
Dans cette VM j'installe dhcp-server
```powershell 
[vagrant@rocky9 ~]$ sudo dnf install dhcp-server
```


# I. Installation d'un serveur DHCP

🌞 **Configurer le serveur DHCP**

```powershell
[vagrant@tp2 ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;

option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 192.168.1.0 netmask 255.255.255.0 {
    
    range 192.168.1.100 192.168.1.200;

    class "pxeclients" {
        match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
        next-server 192.168.1.10; 

        if option architecture-type = 00:07 {
            filename "BOOTX64.EFI";
        }
        else {
            filename "pxelinux.0";
        }
    }
}

```

🌞 **Démarrer le serveur DHCP**


```powershell

[vagrant@tp2 ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; preset: disabled)
     Active: active (running) since Thu 2024-04-04 15:36:14 UTC; 1h 41min ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
   Main PID: 2853 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 12264)
     Memory: 4.5M
        CPU: 11ms
     CGroup: /system.slice/dhcpd.service
             └─2853 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid

Apr 04 15:36:14 tp2.leo dhcpd[2853]:
Apr 04 15:36:14 tp2.leo dhcpd[2853]: No subnet declaration for eth0 (10.0.2.15).
Apr 04 15:36:14 tp2.leo dhcpd[2853]: ** Ignoring requests on eth0.  If this is not what
Apr 04 15:36:14 tp2.leo dhcpd[2853]:    you want, please write a subnet declaration
Apr 04 15:36:14 tp2.leo dhcpd[2853]:    in your dhcpd.conf file for the network segment
Apr 04 15:36:14 tp2.leo dhcpd[2853]:    to which interface eth0 is attached. **
Apr 04 15:36:14 tp2.leo dhcpd[2853]:
Apr 04 15:36:14 tp2.leo dhcpd[2853]: Sending on   Socket/fallback/fallback-net
Apr 04 15:36:14 tp2.leo dhcpd[2853]: Server starting service.
Apr 04 15:36:14 tp2.leo systemd[1]: Started DHCPv4 Server Daemon.

```

🌞 **Démarrer le socket TFTP**

```powershell
[vagrant@tp2 ~]$ sudo systemctl status tftp.socket
● tftp.socket - Tftp Server Activation Socket
     Loaded: loaded (/usr/lib/systemd/system/tftp.socket; enabled; preset: disabled)
     Active: active (listening) since Thu 2024-04-04 16:41:37 UTC; 40min ago
      Until: Thu 2024-04-04 16:41:37 UTC; 40min ago
   Triggers: ● tftp.service
     Listen: [::]:69 (Datagram)
      Tasks: 0 (limit: 12264)
     Memory: 4.0K
        CPU: 549us
     CGroup: /system.slice/tftp.socket

Apr 04 16:41:37 tp2.leo systemd[1]: Listening on Tftp Server Activation Socket.
```

🌞 **Ouvrir le bon port firewall**

```powershell
[vagrant@tp2 ~]$ sudo firewall-cmd --add-service=tftp --permanent
success
[vagrant@tp2 ~]$ sudo firewall-cmd --reload
success
[vagrant@tp2 ~]$

```
# III. Un peu de conf

**Conf du PXE**

```powershell
[vagrant@tp2 ~]$ sudo cp /usr/share/syslinux/pxelinux.0  /var/lib/tftpboot/
[vagrant@tp2 ~]$ sudo mkdir -p /var/pxe/rocky9
[vagrant@tp2 ~]$ sudo mkdir /var/lib/tftpboot/rocky9
[vagrant@tp2 ~]$ sudo mount -t iso9660 -o loop,ro /home/vagrant/Rocky-9.3-x86_64-minimal.iso /var/pxe/rocky9
mount: (hint) your fstab has been modified, but systemd still uses
       the old version; use 'systemctl daemon-reload' to reload.
[vagrant@tp2 ~]$ sudo su
[root@tp2 vagrant] systemctl daemon-reload
[root@tp2 vagrant] cp /var/pxe/rocky9/images/pxeboot/{vmlinuz,initrd.img} /var/lib/tftpboot/rocky9/
[root@tp2 vagrant] cp /usr/share/syslinux/{menu.c32,vesamenu.c32,ldlinux.c32,libcom32.c32,libutil.c32} /var/lib/tftpboot/
[root@tp2 vagrant] mkdir /var/lib/tftpboot/pxelinux.cfg
[root@tp2 vagrant] nano /var/lib/tftpboot/pxelinux.cfg/default
```
 **Conf du `/var/lib/tftpboot/pxelinux.cfg/default`**

```powershell
[vagrant@tp2 ~]$ cat /var/lib/tftpboot/pxelinux.cfg/default
default vesamenu.c32
prompt 1
timeout 60

display boot.msg

label linux
  menu label ^Install Rocky Linux 9 nelokouuuuuuuuuuu
  menu default
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img ip=dhcp inst.repo=http://192.168.1.10/rocky9
label rescue
  menu label ^Rescue installed system
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img rescue
label local
  menu label Boot from ^local drive
  localboot 0xffff
```

**Conf Apache**

```powershell
[root@tp2 vagrant] cat /etc/httpd/conf.d/pxeboot.conf
Alias /rocky9 /var/pxe/rocky9
<Directory /var/pxe/rocky9>
    Options Indexes FollowSymLinks
    Require ip 127.0.0.1 192.168.1.0/24
</Directory>
```

**Ouverture du port 80**

```powershell
[root@tp2 vagrant] sudo firewall-cmd --add-port=80/tcp --permanent
success
[root@tp2 vagrant] sudo firewall-cmd --reload
success
```

# V.TEST


![alt text](image.png)

![alt text](image-1.png)

![alt text](image-2.png)

![alt text](image-3.png)